/**
 * Created by nagarajan on 3/8/16.
 */

'use strict';

/*****************************/
/* Dashboard Page Validation */
/*****************************/
var validateElement = require('./dashboardPageElements.js');
exports.pageElements = function(){
    validateElement.expenseSummaryTable();
    validateElement.alertNotifyTable();
    //validateElement.dashboardAlertLinks();
    //validateElement.dashboardAlertNavigation();
};