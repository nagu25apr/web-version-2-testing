/**
 * Created by nagu on 6/4/16.
 */
'use strict';
var test = require('./end2end.js');
describe('vene.maintenance ', function(){

    xdescribe('should verify different logins used in production for vene.in', function(){
        test.veneVersionTwoUsing('prod.test@vene.in', 'welcome123');          /* for vene.in */
        test.veneVersionTwoUsing('nagusmailbox@gmail.com','Machinetalk1');    /* for vene.in */
        test.veneVersionTwoUsing('bala@machinetalk.io','Machinetalk1');       /* for vene.in */
        test.veneVersionTwoUsing('bala@vene.in','vene123');                   /* for vene.in */
    });

    xdescribe('Seabird Tourists ',function(){
        test.veneVersionTwoUsing('seabird@vene.in', 'password');
    });

    xdescribe('KPN Travels ',function(){
        test.veneVersionTwoUsing('bala@machinetalk.io', 'password');
    });

    xdescribe('Sam Tourist ',function(){
        test.veneVersionTwoUsing('workshop@samtoursntravels.com', 'password');
    });

    xdescribe('SSKP Logistics ',function(){
        test.veneVersionTwoUsing('admin@sskp.com', 'password');
    });

    describe('Infants Travels ',function(){
        test.veneVersionTwoUsing('admin@infants.com', 'password');
    });
});