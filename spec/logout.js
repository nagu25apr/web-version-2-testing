/**
 * Created by nagarajan on 5/4/16.
 */

'use strict';
var loggedIn = require('./login.js');
//var dropDownElem = $$('.uib-dropdown-menu > li');
var dropDownElem = $$('.dropdown-menu > li');
exports.from = function(username){
    loggedIn.profileID.click().then(function(){
        dropDownElem.filter(function(elem){
            return elem.getText().then(function(text){
                return text === 'Log out';
            });
        }).click().then(function(){
            browser.driver.sleep(5000);
            expect(browser.getLocationAbsUrl()).toMatch("/login/signin");
            console.log('Logged Out of '+ username);
            console.log('End to End Test using '+ username +' is completed\n');
            browser.manage().deleteAllCookies();
        });
    });
};