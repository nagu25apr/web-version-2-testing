/**
 * Created by nagu on 6/4/16.
 */

'use strict';

exports.expenseSummaryTable = function(){

    it('should verify if the expense summary table is available', function(){
        var xpensSummaryTable = element(by.css('#page-wrapper > div > div > div > div.col-lg-8 > div:nth-child(2) > table'));
        xpensSummaryTable.isPresent().then(function(){
            console.log('\nExpense Summary Table is Found ....');
        },function(){
            console.log('\nExpense Summary Table Not Found ....');
        });
    });
    it('should verify if the expense summary header is available', function(){
        var xpensSummaryHeader = $('#page-wrapper > div > div > div > div.col-lg-8 > h4:nth-child(1)');
        xpensSummaryHeader.isPresent().then(function(){
            expect(xpensSummaryHeader.getText()).toMatch('Expense Summary');
            console.log('\nExpense Summary Header is Found ....');
        },function(){
            console.log('\nExpense Summary Header Not Found ....');
        });
    });

    var xpensSummaryTableElements = $$('table.table:nth-child(1) > tbody:nth-child(1) > tr');
    it('should verify if the Expense Summary table has the Graph Section',function(){
        xpensSummaryTableElements.get(0).isPresent().then(function(){
            console.log('\nExpense Summary Table Has Graph Section ....');
        },function(){
            console.log('\nGraph Section not found in Expense Summary Table ....');
        });
    });
    it('should verify if the Expense Summary table has the Expense Types Table Header',function(){
        xpensSummaryTableElements.get(1).isPresent().then(function(){
            console.log('\nExpense Summary Table Has Expense Types Table Header ....');
        },function(){
            console.log('\nExpense Types Table Header not found in Expense Summary Table ....');
        });
    });
    it('should verify if the Expense Types table has all available expense types',function(){
        var xpenseTypeRow1 = $$('table.table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(3) > td');
        var xpenseTypeInGraph = $$('.nv-legendWrap > g:nth-child(1) > g:nth-child(1) > g');
        xpenseTypeRow1.get(0).isPresent().then(function(){
            console.log('\nExpense Type '+ xpenseTypeRow1.get(0).getText() +' is available ....');
            expect(xpenseTypeInGraph.get(0).isPresent()).toBeTruthy();
            expect(xpenseTypeInGraph.get(0).getText()).toMatch('Fuel');
        },function(){
            console.log(xpenseTypeInGraph.get(0).getText() + '\n No Expense Type found in Row 1 ....');
            expect(xpenseTypeInGraph.get(0).isPresent()).toBeFalsy();
        });
    });
    iit('should verify if the Expense Types table has all available expense types',function(){
        var xpenseTypeRow = element.all(by.repeater('.expense in vm.expense_values.values'));
        var xpenseTypeCell = element.all(by.tagName('td'));
        console.log('\n'+xpenseTypeCell.get(0).getText()+'is present');
    });
};

exports.alertNotifyTable = function(){

    it('should verify if the alerts table is available', function(){
        var alertTable = element(by.css('#page-wrapper > div > div > div > div.col-lg-4 > div:nth-child(3)'));
        alertTable.isPresent().then(function(){
            console.log('\nAlert Notification Table is Found ....');
        },function(){
            console.log('\nAlert Notification Table Not Found ....');
        });
    });
};

exports.dashboardAlertLinks = function(){
    it('should verify if the alerts links are available in the alert table', function(){
        var alertList = element.all(by.css('a.list-group-item'));
        expect(alertList.count()).toBe(5);
        expect(alertList.get(0).getText()).toMatch('Expired Documents');
        expect(alertList.get(1).getText()).toMatch('Documents due in 15 days');
        expect(alertList.get(2).getText()).toMatch('Expired Licenses');
        //expect(alertList.get(3).getText()).toMatch('Driver Count');  // Removed in Version 2
        expect(alertList.get(3).getText()).toMatch('Open Vehicle Issues');
        expect(alertList.get(4).getText()).toMatch('Open Complaints');
        console.log('tested content in the alerts panel....');
    });
};

exports.dashboardAlertNavigation = function(){
    iit('should validate if the alerts links available in the alert table are working fine', function(){
        var alertList = element.all(by.css('a.list-group-item'));

        alertList.get(0).click().then(function(){
            expect(browser.getLocationAbsUrl()).toMatch("/list/renewaldates");
            //console.log('tested Expired Documents in the alerts panel....OK');
            browser.driver.sleep(5000);
            browser.navigate().back();
            browser.driver.sleep(5000);
        });
        alertList.get(1).click().then(function(){
            expect(browser.getLocationAbsUrl()).toMatch("/list/renewaldates");
            //console.log('tested Due in 15 days Documents in the alerts panel....OK');
            browser.driver.sleep(5000);
            browser.navigate().back();
            browser.driver.sleep(5000);
        });
        alertList.get(2).click().then(function(){
            expect(browser.getLocationAbsUrl()).toMatch("/list/licenseexpired");
            //console.log('tested Expired Licenses in the alerts panel....OK');
            browser.driver.sleep(5000);
            browser.navigate().back();
            browser.driver.sleep(5000);
        });
        // Removed in Version 2
        /*alertList.get(3).click().then(function(){
         expect(browser.getLocationAbsUrl()).toMatch("/list/contact");
         //console.log('tested Driver Count in the alerts panel....OK');
         browser.driver.sleep(5000);
         browser.navigate().back();
         browser.driver.sleep(5000);
         });*/
        alertList.get(3).click().then(function(){
            expect(browser.getLocationAbsUrl()).toMatch("/list/openissue");
            //console.log('tested Open Vehicle Issues in the alerts panel....OK');
            browser.driver.sleep(5000);
            browser.navigate().back();
            browser.driver.sleep(5000);
        });
        alertList.get(4).click().then(function(){
            expect(browser.getLocationAbsUrl()).toMatch("/list/opencomplaint");
            //console.log('tested Open Complaints in the alerts panel....OK');
            browser.driver.sleep(5000);
            browser.navigate().back();
            browser.driver.sleep(5000);
        });
    });
};