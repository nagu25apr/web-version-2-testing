'use strict';

describe('vene.maintenance version 01', function(){
    var paneHeaderList = $$('#wrapper > nav > div.navbar-default.sidebar > div > v-accordion > v-pane');
    var paneContentList = $$('v-pane.is-expanded > v-pane-content > div > v-accordion > v-pane');
    var subLinkList = $$('v-pane.is-expanded > v-pane-content > div > div');

    /* Dash Board Alerts Panel */
    xdescribe('Dashboard alerts panel', function(){
        var alertList = element.all(by.css('a.list-group-item'));
        beforeEach(function(){
            browser.get('/dashboard');
        });
        it('should display the all the alerts that are open / pending', function(){
            expect(alertList.count()).toBe(6);
            expect(alertList.get(0).getText()).toMatch('Expired Documents');
            expect(alertList.get(1).getText()).toMatch('Documents due in 15 days');
            expect(alertList.get(2).getText()).toMatch('Expired Licenses');
            expect(alertList.get(3).getText()).toMatch('Driver Count');
            expect(alertList.get(4).getText()).toMatch('Open Vehicle Issues');
            expect(alertList.get(5).getText()).toMatch('Open Complaints');
            console.log('tested content in the alerts panel....');
        });
        it('should redirect to the renewal dates page when clicked on Expired Documents', function(){
            alertList.get(0).click().then(function(){
                expect(browser.getLocationAbsUrl()).toMatch("/list/renewaldates");
                console.log('tested Expired Documents in the alerts panel....OK');
            });
        });
        it('should redirect to the renewal dates page when clicked on Documents due in 15 days', function(){
            alertList.get(1).click().then(function(){
                expect(browser.getLocationAbsUrl()).toMatch("/list/renewaldates");
                console.log('tested Due in 15 days Documents in the alerts panel....OK');
            });
        });
        it('should redirect to the renewal dates page when clicked on Expired Licenses', function(){
            alertList.get(2).click().then(function(){
                expect(browser.getLocationAbsUrl()).toMatch("/list/licenseexpired");
                console.log('tested Expired Licenses in the alerts panel....OK');
            });
        });
        it('should redirect to the renewal dates page when clicked on Driver Count', function(){
            alertList.get(3).click().then(function(){
                expect(browser.getLocationAbsUrl()).toMatch("/list/contact");
                console.log('tested Driver Count in the alerts panel....OK');
            });
        });
        it('should redirect to the renewal dates page when clicked on Open Vehicle Issues', function(){
            alertList.get(4).click().then(function(){
                expect(browser.getLocationAbsUrl()).toMatch("/list/openissue");
                console.log('tested Open Vehicle Issues in the alerts panel....OK');
            });
        });
        it('should redirect to the renewal dates page when clicked on Open Complaints', function(){
            alertList.get(5).click().then(function(){
                expect(browser.getLocationAbsUrl()).toMatch("/list/opencomplaint");
                console.log('tested Open Complaints in the alerts panel....OK');
            });
        });
    });

    /* Side Bar Panels */
    describe('Side Bar Panel', function(){
        it('should have the pane header names list for the logged in user', function(){
            paneHeaderList.each(function(name, index){
                name.getText().then(function(text){
                    expect(name.isPresent()).toBeTruthy();
                    if (text === 'Dashboard' || text === 'Track' || text === 'Trips' || text === 'Maintenance' ||
                        text === 'Renewals' || text === 'Fuel' || text === 'Complaints' || text === 'Finance' ||
                        text === 'Employees' || text === 'Breakdown/Accident' || text === 'Reports' || text === 'Setup') {
                        console.log(text+ ' link is tested. Available.... Pass');
                    }else {
                        console.log(text +' link tested. Not Available ...');
                    }
                });
            });
        });
    });

    xdescribe('The Track link in the side bar', function(){
        it('should redirect to the vehicle tracking page', function(){
            paneHeaderList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Track';
                });
            }).click().then(function(){
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/gps");
                expect($('#page-wrapper-nomargin > ui-gmap-google-map').isPresent()).toBeTruthy();
            });
        });
    });

    xdescribe('The trips panel in the side bar', function(){
        it('should have the contents listed under trips', function(){
            paneHeaderList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Trips';
                });
            }).click();
            paneContentList.each(function(name, index){
                name.getText().then(function(text){
                    if(text === "Add a Trip") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/entity/new/trip");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "View All Trips") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/trip");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "View All Loads") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/load");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "View Active Trips") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/activetrip");
                        console.log(text+' link tested ... OK');
                    }
                    else {
                        console.log(text +' link tested. Not Available ...');
                    }
                });
            });
        });
    });

    xdescribe('The maintenance panel in the side bar', function(){
        it('should have all contents listed under the maintenance pane', function() {
            paneHeaderList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Maintenance';
                });
            }).click();
            paneContentList.each(function(name, index){
                name.getText().then(function(text){
                    expect(name.isPresent()).toBeTruthy();
                    if (text === 'Service' || text === 'Tyre' || text === 'Inventory') {
                        console.log(text+ ' link is tested. Available.... Pass');
                    }else {
                        console.log(text +' link tested. Not Available ...');
                    }
                });
            });
        });

        it('should have all subLinkList listed under Service', function(){
            paneContentList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Service';
                });
            }).click().then(function(){
                subLinkList.each(function (name, index) {
                    name.getText().then(function (text) {
                        if (name.isPresent()) {
                            if (text === "Add Multiple issues") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/entity/new/bulkissue");
                                console.log(text + ' link tested ... OK');
                            }
                            else if (text === "View All Issues") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/issue");
                                console.log(text + ' link tested ... OK');
                            }
                            else if (text === "View Open Issues") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/openissue");
                                console.log(text + ' link tested ... OK');
                            }
                            else if (text === "View all Jobs") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/job");
                                console.log(text + ' link tested ... OK');
                            }
                            else if (text === "Service Records") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/maintenancerecord");
                                console.log(text + ' link tested ... OK');
                            }
                            else if (text === 'Print Job Card') {
                                console.log(text + ' link is not tested... Reject');
                            }
                            else {
                                console.log(text + ' link tested. Not Available ...');
                            }
                        }
                    });
                });
            });
        });

        it('should have all subLinkList listed under Tyre', function(){
            paneContentList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Tyre';
                });
            }).click().then(function(){
                subLinkList.each(function (name, index) {
                    name.getText().then(function (text) {
                        if (name.isPresent()) {
                            if (text === "View All Tyres") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/tyre");
                                console.log(text + ' link tested ... OK');
                            }
                            else if (text === "View All Fitments") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/tyrefitment");
                                console.log(text + ' link tested ... OK');
                            }
                            else if (text === "View All Retreads") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/tyreretread");
                                console.log(text + ' link tested ... OK');
                            }
                            else if (text === "View Pending Retreads") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/opentyreretread");
                                console.log(text + ' link tested ... OK');
                            }
                            else {
                                console.log(text + ' link tested. Not Available ...');
                            }
                        }
                    });
                });
            });
        });

        it('should have all subLinkList listed under Inventory', function() {
            paneContentList.filter(function (elem, index) {
                return elem.getText().then(function (text) {
                    return text === 'Inventory';
                });
            }).click().then(function(){
                subLinkList.each(function (name, index) {
                    name.getText().then(function (text) {
                        if (name.isPresent()) {
                            if (text === "Current Stock Level") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/stockcount");
                                console.log(text + ' link tested ... OK');
                            }
                            else if (text === "Receipt") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryreceipt");
                                console.log(text + ' link tested ... OK');
                            }
                            else if (text === "Ticket") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryticket");
                                console.log(text + ' link tested ... OK');
                            }
                            else if (text === "Adjustment") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryadjustment");
                                console.log(text + ' link tested ... OK');
                            }
                            else {
                                console.log(text + ' link tested. Not Available ...');
                            }
                        }
                    });
                });
            });
        });
    });

    xdescribe('The renewal panel in the side bar', function(){
        it('should have the contents listed under renewals', function(){
            paneHeaderList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Renewals';
                });
            }).click();
            paneContentList.each(function(name, index){
                name.getText().then(function(text){
                    if(text === "Add a Renewal") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/entity/new/renewal");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "Renewal Status") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/renewaldates");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "View All Renewal Records") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/renewal");
                        console.log(text+' link tested ... OK');
                    }
                    else {
                        console.log(text +' link tested. Not Available ...');
                    }
                });
            });
        });
    });

    xdescribe('The fuel panel in the side bar', function(){
        it('should have the contents listed under fuel', function(){
            paneHeaderList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Fuel';
                });
            }).click();
            paneContentList.each(function(name, index){
                name.getText().then(function(text){
                    if(text === "Add a Fuel Record") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/entity/new/fuel");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "Fuel Records") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/fuel");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "Fuel Economy") console.log(text+ ' link is tested. Available.... Pass. It has subLinkList');
                });
            });
        });

        it('should have all the subLinkList listed under Fuel Economy', function(){
            paneContentList.filter(function (elem, index) {
                return elem.getText().then(function (text) {
                    return text === 'Fuel Economy';
                });
            }).click().then(function(){
                subLinkList.each(function (name, index) {
                    name.getText().then(function (text) {
                        if (name.isPresent()) {
                            if (text === "Drivers") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/fueleconomybydriver");
                                console.log(text + ' link tested ... OK');
                            }
                            else if (text === "Vehicles") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/fueleconomybyvehicle");
                                console.log(text + ' link tested ... OK');
                            }
                            else {
                                console.log(text + ' link tested. Not Available ...');
                            }
                        }
                    });
                });
            });
        });
    });

    xdescribe('The complaints panel in the side bar', function(){
        it('should have the contents listed under complaints', function(){
            paneHeaderList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Complaints';
                });
            }).click();
            paneContentList.each(function(name, index){
                name.getText().then(function(text){
                    if(text === "View All Complaints") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/complaint");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "View Open Complaints") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/opencomplaint");
                        console.log(text+' link tested ... OK');
                    }
                    else {
                        console.log(text +' link tested. Not Available ...');
                    }
                });
            });
        });
    });

    xdescribe('The finance panel in the side bar', function(){
        it('should have the contents listed under finance', function(){
            paneHeaderList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Finance';
                });
            }).click();
            paneContentList.each(function(name, index){
                name.getText().then(function(text){
                    if(text === "Add an Expense") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/entity/new/expense");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "Add Revenue") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/entity/new/revenue");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "View All Expenses") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/expense");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "View All Revenue") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/revenue");
                        console.log(text+' link tested ... OK');
                    }
                    else {
                        console.log(text +' link tested. Not Available ...');
                    }
                });
            });
        });
    });

    xdescribe('The employees panel in the side bar', function(){
        it('should have the contents listed under employees', function(){
            paneHeaderList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Employees';
                });
            }).click();
            paneContentList.each(function(name, index){
                name.getText().then(function(text){
                    if(text === "Add an Employee") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/entity/new/contact");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "View All Employees") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/contact");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "Bata Transactions") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/driverbata");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "Bata Summary") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/batasummary");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "License Expiring Soon") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/licenseexpiring");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "License Expired Already") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/licenseexpired");
                        console.log(text+' link tested ... OK');
                    }
                    else {
                        console.log(text +' link tested. Not Available ...');
                    }
                });
            });
        });
    });

    xdescribe('The Breakdown/Accident panel in the side bar', function(){
        it('should have the contents listed under the panel', function(){
            paneHeaderList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Breakdown/Accident';
                });
            }).click();
            paneContentList.each(function(name, index){
                name.getText().then(function(text){
                    if(text === "Add a Breakdown") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/entity/new/breakdown");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "View All Incidents") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/breakdown");
                        console.log(text+' link tested ... OK');
                    }
                    else {
                        console.log(text +' link tested. Not Available ...');
                    }
                });
            });
        });
    });

    xdescribe('The Reports panel in the side bar', function(){
        it('should have the contents listed under the panel', function(){
            paneHeaderList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Reports';
                });
            }).click();
            paneContentList.each(function(name, index){
                name.getText().then(function(text){
                    if(text === "Vehicle Monthly Report") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/vehiclemonthlyreport");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "Vehicle Tracking Summary") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/gps/report");
                        console.log(text+' link tested ... OK');
                    }
                    else {
                        console.log(text +' link tested. Not Available ...');
                    }
                });
            });
        });
    });

    xdescribe('The Setup panel in the side bar', function(){
        it('should have all contents listed under the setup panel', function() {
            paneHeaderList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Setup';
                });
            }).click();
            paneContentList.each(function(name, index){
                name.getText().then(function(text){
                    expect(name.isPresent()).toBeTruthy();
                    if  (
                        text === 'Company' || text === 'Vehicle' || text === 'Consignment' ||
                        text === 'Customer' || text === 'Location' || text === 'Trip' ||
                        text === 'Tyre' || text === 'Inventory' || text === 'Service' ||
                        text === 'Renewal' || text === 'Fuel' || text === 'Route' ||
                        text === 'Finance' || text === 'Employee'
                        ) {
                        console.log(text+ ' link is tested. Available.... Pass');
                    }
                    else {
                        console.log(text +' link tested. Not Available ...');
                    }
                });
            });
        });

        it('should have all subLinkList listed under Company', function(){
            paneContentList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Company';
                });
            }).click().then(function() {
                subLinkList.each(function (name, index) {
                    name.getText().then(function (text) {
                        if (name.isPresent()) {
                            if (text === "Branches") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/companylocation");
                                console.log(text + ' link tested ... OK');
                            }
                            else {
                                console.log(text + ' link tested. Not Available ...');
                            }
                        }
                    });
                });
            });
        });

        it('should have all subLinkList listed under Vehicle', function(){
            paneContentList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Vehicle';
                });
            }).click().then(function(){
                subLinkList.each(function(name,index){
                    name.getText().then(function(text){
                        if(name.isPresent()) {
                            if (text === "Vehicle") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/vehicle");
                                console.log(text+' link tested ... OK');
                            }
                            else if (text === "Vehicle Make") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/vehiclemake");
                                console.log(text+' link tested ... OK');
                            }
                            else if (text === "Vehicle Year") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/vehicleyear");
                                console.log(text+' link tested ... OK');
                            }
                            else if (text === "Vehicle Group") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/vehiclegroup");
                                console.log(text +' link tested ... OK');
                            }
                            else if (text === "Fuel Group") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/fuelgroup");
                                console.log(text +' link tested ... OK');
                            }
                            else{
                                console.log(text +' link tested. Not Available ...');
                            }
                        }
                    });
                });
            });
        });

        it('should have all subLinkList listed under Consignment', function(){
            paneContentList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Consignment';
                });
            }).click().then(function(){
                subLinkList.each(function(name,index){
                    name.getText().then(function(text){
                        if(name.isPresent()) {
                            if (text === "Master Settings") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/articletype");
                                console.log(text+' link tested ... OK');
                            }
                            else if (text === "Article Type") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/articletype");
                                console.log(text+' link tested ... OK');
                            }
                            else{
                                console.log(text +' link tested. Not Available ...');
                            }
                        }
                    });
                });
            });
        });

        it('should have all subLinkList listed under Customer', function(){
            paneContentList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Customer';
                });
            }).click().then(function(){
                subLinkList.each(function(name,index){
                    name.getText().then(function(text){
                        if(name.isPresent()) {
                            if (text === "Customer") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/customer");
                                console.log(text+' link tested ... OK');
                            }
                            else{
                                console.log(text +' link tested. Not Available ...');
                            }
                        }
                    });
                });
            });
        });

        it('should have all subLinkList listed under Location', function(){
            paneContentList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Location';
                });
            }).click().then(function(){
                subLinkList.each(function(name,index){
                    name.getText().then(function(text){
                        if(name.isPresent()) {
                            if (text === "Station") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/station");
                                console.log(text+' link tested ... OK');
                            }
                            else{
                                console.log(text +' link tested. Not Available ...');
                            }
                        }
                    });
                });
            });
        });

        it('should have all subLinkList listed under Trip', function(){
            paneContentList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Trip';
                });
            }).click().then(function(){
                subLinkList.each(function(name,index){
                    name.getText().then(function(text){
                        if(name.isPresent()) {
                            if (text === "Trip Expense Type") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/tripexpensetype");
                                console.log(text+' link tested ... OK');
                            }
                            else{
                                console.log(text +' link tested. Not Available ...');
                            }
                        }
                    });
                });
            });
        });

        it('should have all subLinkList listed under Tyre', function(){
            paneContentList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Tyre';
                });
            }).click().then(function(){
                subLinkList.each(function(name,index){
                    name.getText().then(function(text){
                        if(name.isPresent()) {
                            if (text === "Tyre Make") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/tyremake");
                                console.log(text+' link tested ... OK');
                            }
                            else if (text === "Tyre Size") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/tyresize");
                                console.log(text+' link tested ... OK');
                            }
                            else if (text === "Tyre Vendor") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/tyrevendor");
                                console.log(text+' link tested ... OK');
                            }
                            else if (text === "Purchase Condition") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/purchasecondition");
                                console.log(text +' link tested ... OK');
                            }
                            else if (text === "Tyre Position") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/tyreposition");
                                console.log(text +' link tested ... OK');
                            }
                            else{
                                console.log(text +' link tested. Not Available ...');
                            }
                        }
                    });
                });
            });
        });

        it('should have all subLinkList listed under Inventory', function(){
            paneContentList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Inventory';
                });
            }).click().then(function(){
                subLinkList.each(function(name,index){
                    name.getText().then(function(text){
                        if(name.isPresent()) {
                            if (text === "Inventory Item") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryitem");
                                console.log(text+' link tested ... OK');
                            }
                            else if (text === "Payment Type") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/inventorypaymenttype");
                                console.log(text+' link tested ... OK');
                            }
                            else if (text === "Vendor") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryvendor");
                                console.log(text+' link tested ... OK');
                            }
                            else if (text === "Unit of Measure") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryuom");
                                console.log(text +' link tested ... OK');
                            }
                            else if (text === "Make") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/inventorymake");
                                console.log(text +' link tested ... OK');
                            }
                            else if (text === "Location") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/inventorylocation");
                                console.log(text +' link tested ... OK');
                            }
                            else if (text === "Adjustment Reason") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryadjustmentreason");
                                console.log(text +' link tested ... OK');
                            }
                            else if (text === "Item Group") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryitemgroup");
                                console.log(text +' link tested ... OK');
                            }
                            else if (text === "Item Category") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryitemcategory");
                                console.log(text +' link tested ... OK');
                            }
                            else if (text === "Item Sub Category") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryitemsubcategory");
                                console.log(text +' link tested ... OK');
                            }
                            else{
                                console.log(text +' link tested. Not Available ...');
                            }
                        }
                    });
                });
            });
        });

        it('should have all subLinkList listed under Service', function(){
            paneContentList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Service';
                });
            }).click().then(function(){
                subLinkList.each(function(name,index){
                    name.getText().then(function(text){
                        if(name.isPresent()) {
                            if (text === "Scheduled Tasks") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/scheduledtasks");
                                console.log(text+' link tested ... OK');
                            }
                            else if (text === "Service Task") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/servicetask");
                                console.log(text+' link tested ... OK');
                            }
                            else if (text === "Service Task Group") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/servicetaskgroup");
                                console.log(text+' link tested ... OK');
                            }
                            else if (text === "Maintenance Groups") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/maintenancegroup");
                                console.log(text +' link tested ... OK');
                            }
                            else if (text === "Service Vendor") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/maintenancevendor");
                                console.log(text +' link tested ... OK');
                            }
                            else if (text === "Maintenance Type") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/maintenancetype");
                                console.log(text +' link tested ... OK');
                            }
                            else{
                                console.log(text +' link tested. Not Available ...');
                            }
                        }
                    });
                });
            });
        });

        it('should have all subLinkList listed under Renewal', function(){
            paneContentList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Renewal';
                });
            }).click().then(function(){
                subLinkList.each(function(name,index){
                    name.getText().then(function(text){
                        if(name.isPresent()) {
                            if (text === "Renewal Type") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/renewaltype");
                                console.log(text+' link tested ... OK');
                            }
                            else{
                                console.log(text +' link tested. Not Available ...');
                            }
                        }
                    });
                });
            });
        });

        it('should have all subLinkList listed under Fuel', function(){
            paneContentList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Fuel';
                });
            }).click().then(function(){
                subLinkList.each(function(name,index){
                    name.getText().then(function(text){
                        if(name.isPresent()) {
                            if (text === "Vendor") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/fuelvendor");
                                console.log(text+' link tested ... OK');
                            }
                            else if (text === "Payment Type") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/fuelpaymenttype");
                                console.log(text+' link tested ... OK');
                            }
                            else{
                                console.log(text +' link tested. Not Available ...');
                            }
                        }
                    });
                });
            });
        });

        it('should have all subLinkList listed under Route', function(){
            paneContentList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Route';
                });
            }).click().then(function(){
                subLinkList.each(function(name,index){
                    name.getText().then(function(text){
                        if(name.isPresent()) {
                            if (text === "Route") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/route");
                                console.log(text+' link tested ... OK');
                            }
                            else{
                                console.log(text +' link tested. Not Available ...');
                            }
                        }
                    });
                });
            });
        });

        it('should have all subLinkList listed under Finance', function(){
            paneContentList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Finance';
                });
            }).click().then(function(){
                subLinkList.each(function(name,index){
                    name.getText().then(function(text){
                        if(name.isPresent()) {
                            if (text === "Expense Type") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/expensetype");
                                console.log(text+' link tested ... OK');
                            }
                            else if (text === "Revenue Type") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/revenuetype");
                                console.log(text+' link tested ... OK');
                            }
                            else{
                                console.log(text +' link tested. Not Available ...');
                            }
                        }
                    });
                });
            });
        });

        it('should have all subLinkList listed under Employee', function(){
            paneContentList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Employee';
                });
            }).click().then(function(){
                subLinkList.each(function(name,index){
                    name.getText().then(function(text){
                        if(name.isPresent()) {
                            if (text === "Employee Roles") {
                                name.click();
                                browser.driver.sleep(5000);
                                expect(browser.getLocationAbsUrl()).toMatch("/list/contacttype");
                                console.log(text+' link tested ... OK');
                            }
                            else{
                                console.log(text +' link tested. Not Available ...');
                            }
                        }
                    });
                });
            });
        });
    });

    describe('The Dashboard link in the side bar', function(){
        it('should redirect to the dashboard page', function(){
            paneHeaderList.filter(function(elem, index) {
                return elem.getText().then(function(text) {
                    return text === 'Dashboard';
                });
            }).click().then(function(){
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/dashboard");
            });
        });
    });
});