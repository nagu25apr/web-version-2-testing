/**
 * Created by nagarajan on 5/4/16.
 */

var paneHeaderList = $$('#wrapper > nav > div.navbar-default.sidebar > div > v-accordion > v-pane');
var paneContentList = $$('v-pane.is-expanded > v-pane-content > div > v-accordion > v-pane');
var subLinkList = $$('v-pane.is-expanded > v-pane-content > div > div');

exports.sidebarContents = function(){
    paneHeaderList.each(function(name, index){
        name.getText().then(function(text){
            expect(name.isPresent()).toBeTruthy();
            if (text === 'Dashboard' || text === 'Track' || text === 'Trips' || text === 'Maintenance' ||
                text === 'Renewals' || text === 'Fuel' || text === 'Complaints' || text === 'Finance' ||
                text === 'Employees' || text === 'Breakdown/Accident' || text === 'Reports' || text === 'Setup') {
                console.log(text+ ' link is available in the sidebar.... Pass');
            }else {
                console.log(text +' link tested. Not Available ...');
            }
        });
    });
};

exports.vehicleTrackLink = function(){
    paneHeaderList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Tracking';
        });
    }).click();
    paneContentList.each(function(name, index){
        name.getText().then(function(text){
            if(text === "Track Vehicles") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/gps");
                expect(element(by.css('.angular-google-map-container')).isPresent()).toBeTruthy();
            }else {
                console.log(text +' link tested. Not Available ...');
            }
        });
    });
};

exports.tripMenuItems = function(){
    paneHeaderList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Trips';
        });
    }).click();
    paneContentList.each(function(name, index){
        name.getText().then(function(text){
            if(text === "Add a Trip") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/entity/new/trip");
                console.log(text+' link tested ... OK');
            }
            else if(text === "View All Trips") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/trip");
                console.log(text+' link tested ... OK');
            }
            else if(text === "View All Loads") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/load");
                console.log(text+' link tested ... OK');
            }
            else if(text === "View Active Trips") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/activetrip");
                console.log(text+' link tested ... OK');
            }
            else if(text === "View All Services") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/service");
                console.log(text+' link tested ... OK');
            }
            else if(text === "Booking") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/calendar/booking");
                console.log(text+' link tested ... OK');
            }
            else if(text === "Upload Tripsheets") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("upload/tripsheet");
                console.log(text+' link tested ... OK');
            }
            else if(text === "View Tripsheets") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/report/tripsheet");
                console.log(text+' link tested ... OK');
            }
            else {
                console.log(text +' link tested. Not Available ...');
            }
        });
    });
};

exports.tyreMenuItems = function(){
    paneHeaderList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Tyres';
        });
    }).click();
    paneContentList.each(function(name, index){
        name.getText().then(function(text){
            if(text === "Add a Tyre") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/entity/new/tyre");
                console.log(text+' link tested ... OK');
            }
            else if(text === "View Tyres") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/tyre");
                console.log(text+' link tested ... OK');
            }
            else if(text === "View Fitments") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/tyrefitment");
                console.log(text+' link tested ... OK');
            }
            else if(text === "View Retreads & Patches") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/tyreretread");
                console.log(text+' link tested ... OK');
            }
            else if(text === "View Pending Retreads") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/opentyreretread");
                console.log(text+' link tested ... OK');
            }
            else if(text === "Fitments by Tyre") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/report/tyrefitments");
                console.log(text+' link tested ... OK');
            }
            else if(text === "Fitments by Vehicle") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/report/vehicletyrefitments");
                console.log(text+' link tested ... OK');
            }
            else {
                console.log(text +' link tested. Not Available ...');
            }
        });
    });
};

exports.maintenanceMenuItems = function(){
    paneHeaderList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Maintenance';
        });
    }).click();
    paneContentList.each(function(name, index){
        name.getText().then(function(text){
            expect(name.isPresent()).toBeTruthy();
            if (text === 'Service' || text === 'Tyre' || text === 'Inventory') {
                console.log(text+ ' link is tested. Available.... Pass');
            }else {
                console.log(text +' link tested. Not Available ...');
            }
        });
    });
};

exports.maintenanceServiceMenuItems = function(){
    paneContentList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Service';
        });
    }).click().then(function(){
        subLinkList.each(function (name, index) {
            name.getText().then(function (text) {
                if (name.isPresent()) {
                    if (text === "Add Multiple issues") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/entity/new/bulkissue");
                        console.log(text + ' link tested ... OK');
                    }
                    else if (text === "View All Issues") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/issue");
                        console.log(text + ' link tested ... OK');
                    }
                    else if (text === "View Open Issues") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/openissue");
                        console.log(text + ' link tested ... OK');
                    }
                    else if (text === "View all Jobs") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/job");
                        console.log(text + ' link tested ... OK');
                    }
                    else if (text === "Service Records") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/maintenancerecord");
                        console.log(text + ' link tested ... OK');
                    }
                    else if (text === 'Print Job Card') {
                        console.log(text + ' link is not tested... Reject');
                    }
                    else {
                        console.log(text + ' link tested. Not Available ...');
                    }
                }
            });
        });
    });
};

exports.maintenanceTyreMenuItems = function(){
    paneContentList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Tyre';
        });
    }).click().then(function(){
        subLinkList.each(function (name, index) {
            name.getText().then(function (text) {
                if (name.isPresent()) {
                    if(text === "View Tyres") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/tyre");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "View Fitments") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/tyrefitment");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "View Retreads & Patches") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/tyreretread");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "View Pending Retreads") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/opentyreretread");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "Fitments by Tyre") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/tyrefitments");
                        console.log(text+' link tested ... OK');
                    }
                    else if(text === "Fitments by Vehicle") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/vehicletyrefitments");
                        console.log(text+' link tested ... OK');
                    }
                    else {
                        console.log(text + ' link tested. Not Available ...');
                    }
                }
            });
        });
    });
};

exports.maintenanceInventoryMenuItems = function(){
    paneContentList.filter(function (elem, index) {
        return elem.getText().then(function (text) {
            return text === 'Inventory';
        });
    }).click().then(function(){
        subLinkList.each(function (name, index) {
            name.getText().then(function (text) {
                if (name.isPresent()) {
                    if (text === "Current Stock Level") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/stockcount");
                        console.log(text + ' link tested ... OK');
                    }
                    else if (text === "Receipt") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryreceipt");
                        console.log(text + ' link tested ... OK');
                    }
                    else if (text === "Ticket") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryticket");
                        console.log(text + ' link tested ... OK');
                    }
                    else if (text === "Adjustment") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryadjustment");
                        console.log(text + ' link tested ... OK');
                    }
                    else {
                        console.log(text + ' link tested. Not Available ...');
                    }
                }
            });
        });
    });
};

exports.renewalMenuItems = function(){
    paneHeaderList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Renewals';
        });
    }).click();
    paneContentList.each(function(name, index){
        name.getText().then(function(text){
            if(text === "Add a Renewal") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/entity/new/renewal");
                console.log(text+' link tested ... OK');
            }
            else if(text === "Renewal Status") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/renewaldates");
                console.log(text+' link tested ... OK');
            }
            else if(text === "View All Renewal Records") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/renewal");
                console.log(text+' link tested ... OK');
            }
            else {
                console.log(text +' link tested. Not Available ...');
            }
        });
    });
};

exports.fuelMenuItems = function(){
    paneHeaderList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Fuel';
        });
    }).click();
    paneContentList.each(function(name, index){
        name.getText().then(function(text){
            if(text === "Add a Fuel Record") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/entity/new/fuel");
                console.log(text+' link tested ... OK');
            }
            else if(text === "Fuel Records") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/fuel");
                console.log(text+' link tested ... OK');
            }
            else if(text === "Fuel Economy") console.log(text+ ' link is tested. Available.... Pass. It has subLinkList');
        });
    });
};

exports.fuelEconomyMenuItems = function(){
    paneContentList.filter(function (elem, index) {
        return elem.getText().then(function (text) {
            return text === 'Fuel Economy';
        });
    }).click().then(function(){
        subLinkList.each(function (name, index) {
            name.getText().then(function (text) {
                if (name.isPresent()) {
                    if (text === "Drivers") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/fueleconomybydriver");
                        console.log(text + ' link tested ... OK');
                    }
                    else if (text === "Vehicles") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/fueleconomybyvehicle");
                        console.log(text + ' link tested ... OK');
                    }
                    else {
                        console.log(text + ' link tested. Not Available ...');
                    }
                }
            });
        });
    });
};

exports.complaintsMenuItems = function(){
    paneHeaderList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Complaints';
        });
    }).click();
    paneContentList.each(function(name, index){
        name.getText().then(function(text){
            if(text === "View All Complaints") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/complaint");
                console.log(text+' link tested ... OK');
            }
            else if(text === "View Open Complaints") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/opencomplaint");
                console.log(text+' link tested ... OK');
            }
            else {
                console.log(text +' link tested. Not Available ...');
            }
        });
    });
};

exports.financeMenuItems = function(){
    paneHeaderList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Finance';
        });
    }).click();
    paneContentList.each(function(name, index){
        name.getText().then(function(text){
            if(text === "Add an Expense") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/entity/new/expense");
                console.log(text+' link tested ... OK');
            }
            else if(text === "Add Revenue") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/entity/new/revenue");
                console.log(text+' link tested ... OK');
            }
            else if(text === "View All Expenses") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/expense");
                console.log(text+' link tested ... OK');
            }
            else if(text === "View All Revenue") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/revenue");
                console.log(text+' link tested ... OK');
            }
            else {
                console.log(text +' link tested. Not Available ...');
            }
        });
    });
};

exports.employeesMenuItems = function(){
    paneHeaderList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Employees';
        });
    }).click();
    paneContentList.each(function(name, index){
        name.getText().then(function(text){
            if(text === "Add an Employee") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/entity/new/contact");
                console.log(text+' link tested ... OK');
            }
            else if(text === "View All Employees") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/contact");
                console.log(text+' link tested ... OK');
            }
            else if(text === "Bata Transactions") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/driverbata");
                console.log(text+' link tested ... OK');
            }
            else if(text === "Bata Summary") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/batasummary");
                console.log(text+' link tested ... OK');
            }
            else if(text === "License Expiring Soon") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/licenseexpiring");
                console.log(text+' link tested ... OK');
            }
            else if(text === "License Expired Already") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/licenseexpired");
                console.log(text+' link tested ... OK');
            }
            else {
                console.log(text +' link tested. Not Available ...');
            }
        });
    });
};

exports.breakdownMenuItems = function(){
    paneHeaderList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Breakdown/Accident';
        });
    }).click();
    paneContentList.each(function(name, index){
        name.getText().then(function(text){
            if(text === "Add a Breakdown") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/entity/new/breakdown");
                console.log(text+' link tested ... OK');
            }
            else if(text === "View All Incidents") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/breakdown");
                console.log(text+' link tested ... OK');
            }
            else {
                console.log(text +' link tested. Not Available ...');
            }
        });
    });
};

exports.reportsMenuItems = function(){
    paneHeaderList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Reports';
        });
    }).click();
    paneContentList.each(function(name, index){
        name.getText().then(function(text){
            if(text === "Vehicle Monthly Report") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/list/vehiclemonthlyreport");
                console.log(text+' link tested ... OK');
            }
            else if(text === "Vehicle Tracking Summary") {
                name.click();
                browser.driver.sleep(5000);
                expect(browser.getLocationAbsUrl()).toMatch("/gps/report");
                console.log(text+' link tested ... OK');
            }
            else {
                console.log(text +' link tested. Not Available ...');
            }
        });
    });
};

exports.setupMenuItems = function(){
    paneHeaderList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Setup';
        });
    }).click();
    paneContentList.each(function(name, index){
        name.getText().then(function(text){
            expect(name.isPresent()).toBeTruthy();
            if  (
                text === 'Company' || text === 'Vehicle' || text === 'Consignment' ||
                text === 'Customer' || text === 'Location' || text === 'Trip' ||
                text === 'Tyre' || text === 'Inventory' || text === 'Service' ||
                text === 'Renewal' || text === 'Fuel' || text === 'Route' ||
                text === 'Finance' || text === 'Employee'
            ) {
                console.log(text+ ' link is tested. Available.... Pass');
            }
            else {
                console.log(text +' link tested. Not Available ...');
            }
        });
    });
};

exports.companySetupMenuItems = function(){
    paneContentList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Company';
        });
    }).click().then(function() {
        subLinkList.each(function (name, index) {
            name.getText().then(function (text) {
                if (name.isPresent()) {
                    if (text === "Branches") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/companylocation");
                        console.log(text + ' link tested ... OK');
                    }
                    else {
                        console.log(text + ' link tested. Not Available ...');
                    }
                }
            });
        });
    });
};

exports.vehicleSetupMenuItems = function(){
    paneContentList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Vehicle';
        });
    }).click().then(function(){
        subLinkList.each(function(name,index){
            name.getText().then(function(text){
                if(name.isPresent()) {
                    if (text === "Vehicle") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/vehicle");
                        console.log(text+' link tested ... OK');
                    }
                    else if (text === "Vehicle Make") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/vehiclemake");
                        console.log(text+' link tested ... OK');
                    }
                    else if (text === "Vehicle Year") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/vehicleyear");
                        console.log(text+' link tested ... OK');
                    }
                    else if (text === "Vehicle Group") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/vehiclegroup");
                        console.log(text +' link tested ... OK');
                    }
                    else if (text === "Fuel Group") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/fuelgroup");
                        console.log(text +' link tested ... OK');
                    }
                    else{
                        console.log(text +' link tested. Not Available ...');
                    }
                }
            });
        });
    });
};

exports.consignmentSetupMenuItems = function(){
    paneContentList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Consignment';
        });
    }).click().then(function(){
        subLinkList.each(function(name,index){
            name.getText().then(function(text){
                if(name.isPresent()) {
                    if (text === "Master Settings") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/articletype");
                        console.log(text+' link tested ... OK');
                    }
                    else if (text === "Article Type") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/articletype");
                        console.log(text+' link tested ... OK');
                    }
                    else{
                        console.log(text +' link tested. Not Available ...');
                    }
                }
            });
        });
    });
};

exports.customerSetupMenuItems = function(){
    paneContentList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Customer';
        });
    }).click().then(function(){
        subLinkList.each(function(name,index){
            name.getText().then(function(text){
                if(name.isPresent()) {
                    if (text === "Customer") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/customer");
                        console.log(text+' link tested ... OK');
                    }
                    else{
                        console.log(text +' link tested. Not Available ...');
                    }
                }
            });
        });
    });
};

exports.locationSetupMenuItems = function(){
    paneContentList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Location';
        });
    }).click().then(function(){
        subLinkList.each(function(name,index){
            name.getText().then(function(text){
                if(name.isPresent()) {
                    if (text === "Station") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/station");
                        console.log(text+' link tested ... OK');
                    }
                    else{
                        console.log(text +' link tested. Not Available ...');
                    }
                }
            });
        });
    });
};

exports.tripSetupMenuItems = function(){
    paneContentList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Trip';
        });
    }).click().then(function(){
        subLinkList.each(function(name,index){
            name.getText().then(function(text){
                if(name.isPresent()) {
                    if (text === "Trip Expense Type") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/tripexpensetype");
                        console.log(text+' link tested ... OK');
                    }
                    else{
                        console.log(text +' link tested. Not Available ...');
                    }
                }
            });
        });
    });
};

exports.tyreSetupMenuItems = function(){
    paneContentList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Tyre';
        });
    }).click().then(function(){
        subLinkList.each(function(name,index){
            name.getText().then(function(text){
                if(name.isPresent()) {
                    if (text === "Tyre Make") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/tyremake");
                        console.log(text+' link tested ... OK');
                    }
                    else if (text === "Tyre Size") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/tyresize");
                        console.log(text+' link tested ... OK');
                    }
                    else if (text === "Tyre Vendor") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/tyrevendor");
                        console.log(text+' link tested ... OK');
                    }
                    else if (text === "Purchase Condition") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/purchasecondition");
                        console.log(text +' link tested ... OK');
                    }
                    else if (text === "Tyre Position") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/tyreposition");
                        console.log(text +' link tested ... OK');
                    }
                    else{
                        console.log(text +' link tested. Not Available ...');
                    }
                }
            });
        });
    });
};

exports.inventorySetupMenuItems = function(){
    paneContentList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Inventory';
        });
    }).click().then(function(){
        subLinkList.each(function(name,index){
            name.getText().then(function(text){
                if(name.isPresent()) {
                    if (text === "Inventory Item") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryitem");
                        console.log(text+' link tested ... OK');
                    }
                    else if (text === "Payment Type") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/inventorypaymenttype");
                        console.log(text+' link tested ... OK');
                    }
                    else if (text === "Vendor") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryvendor");
                        console.log(text+' link tested ... OK');
                    }
                    else if (text === "Unit of Measure") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryuom");
                        console.log(text +' link tested ... OK');
                    }
                    else if (text === "Make") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/inventorymake");
                        console.log(text +' link tested ... OK');
                    }
                    else if (text === "Location") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/inventorylocation");
                        console.log(text +' link tested ... OK');
                    }
                    else if (text === "Adjustment Reason") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryadjustmentreason");
                        console.log(text +' link tested ... OK');
                    }
                    else if (text === "Item Group") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryitemgroup");
                        console.log(text +' link tested ... OK');
                    }
                    else if (text === "Item Category") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryitemcategory");
                        console.log(text +' link tested ... OK');
                    }
                    else if (text === "Item Sub Category") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/inventoryitemsubcategory");
                        console.log(text +' link tested ... OK');
                    }
                    else{
                        console.log(text +' link tested. Not Available ...');
                    }
                }
            });
        });
    });
};

exports.serviceSetupMenuItems = function(){
    paneContentList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Service';
        });
    }).click().then(function(){
        subLinkList.each(function(name,index){
            name.getText().then(function(text){
                if(name.isPresent()) {
                    if (text === "Scheduled Tasks") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/scheduledtasks");
                        console.log(text+' link tested ... OK');
                    }
                    else if (text === "Service Task") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/servicetask");
                        console.log(text+' link tested ... OK');
                    }
                    else if (text === "Service Task Group") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/servicetaskgroup");
                        console.log(text+' link tested ... OK');
                    }
                    else if (text === "Maintenance Groups") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/maintenancegroup");
                        console.log(text +' link tested ... OK');
                    }
                    else if (text === "Service Vendor") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/maintenancevendor");
                        console.log(text +' link tested ... OK');
                    }
                    else if (text === "Maintenance Type") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/maintenancetype");
                        console.log(text +' link tested ... OK');
                    }
                    else{
                        console.log(text +' link tested. Not Available ...');
                    }
                }
            });
        });
    });
};

exports.renewalSetupMenuItems = function(){
    paneContentList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Renewal';
        });
    }).click().then(function(){
        subLinkList.each(function(name,index){
            name.getText().then(function(text){
                if(name.isPresent()) {
                    if (text === "Renewal Type") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/renewaltype");
                        console.log(text+' link tested ... OK');
                    }
                    else{
                        console.log(text +' link tested. Not Available ...');
                    }
                }
            });
        });
    });
};

exports.fuelSetupMenuItems = function(){
    paneContentList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Fuel';
        });
    }).click().then(function(){
        subLinkList.each(function(name,index){
            name.getText().then(function(text){
                if(name.isPresent()) {
                    if (text === "Vendor") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/fuelvendor");
                        console.log(text+' link tested ... OK');
                    }
                    else if (text === "Payment Type") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/fuelpaymenttype");
                        console.log(text+' link tested ... OK');
                    }
                    else{
                        console.log(text +' link tested. Not Available ...');
                    }
                }
            });
        });
    });
};

exports.routeSetupMenuItems = function(){
    paneContentList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Route';
        });
    }).click().then(function(){
        subLinkList.each(function(name,index){
            name.getText().then(function(text){
                if(name.isPresent()) {
                    if (text === "Route") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/route");
                        console.log(text+' link tested ... OK');
                    }
                    else{
                        console.log(text +' link tested. Not Available ...');
                    }
                }
            });
        });
    });
};

exports.financeSetupMenuItems = function(){
    paneContentList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Finance';
        });
    }).click().then(function(){
        subLinkList.each(function(name,index){
            name.getText().then(function(text){
                if(name.isPresent()) {
                    if (text === "Expense Type") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/expensetype");
                        console.log(text+' link tested ... OK');
                    }
                    else if (text === "Revenue Type") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/revenuetype");
                        console.log(text+' link tested ... OK');
                    }
                    else{
                        console.log(text +' link tested. Not Available ...');
                    }
                }
            });
        });
    });
};

exports.employeeSetupMenuItems = function(){
    paneContentList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Employee';
        });
    }).click().then(function(){
        subLinkList.each(function(name,index){
            name.getText().then(function(text){
                if(name.isPresent()) {
                    if (text === "Employee Roles") {
                        name.click();
                        browser.driver.sleep(5000);
                        expect(browser.getLocationAbsUrl()).toMatch("/list/contacttype");
                        console.log(text+' link tested ... OK');
                    }
                    else{
                        console.log(text +' link tested. Not Available ...');
                    }
                }
            });
        });
    });
};

exports.dashboardLink = function(){
    paneHeaderList.filter(function(elem, index) {
        return elem.getText().then(function(text) {
            return text === 'Dashboard';
        });
    }).click().then(function(){
        browser.driver.sleep(5000);
        expect(browser.getLocationAbsUrl()).toMatch("/dashboard");
    });
};