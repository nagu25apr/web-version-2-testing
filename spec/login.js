/**
 * Created by nagu on 4/4/16.
 */

'use strict';
exports.using = function(username, password){

    //browser.get('/');
    expect(browser.getLocationAbsUrl()).toMatch("/login/signin");

    this.userName = element(by.model('credentials.username')).clear();
    this.passWord = element(by.model('credentials.password')).clear();
    //this.profileID = element(by.id('simple-dropdown')); /* used in Version 1 - vene.in */
    this.profileID = element(by.css('#btn-append-to-single-button'));
    this.loginButton = element(by.css('.btn'));

    this.loginFunction = function(uid, pwd){
        this.userName.sendKeys(uid);
        this.passWord.sendKeys(pwd);
        this.loginButton.click();
        browser.driver.sleep(5000);
    };

    /*if(username === 'prod.test@vene.in') {
        this.loginFunction(username, password);
        this.profileID.getText().then(function(name){
            expect(name).toMatch('Vene Testing');
            console.log('Logged in as '+ name);
            expect(element(by.css('.navbar-title')).getText()).toMatch("VENE DEMO");
        });
    }*/

    /*if(username === 'nagusmailbox@gmail.com') {
        this.loginFunction(username, password);
        this.profileID.getText().then(function(name){
            expect(name).toMatch('Nagarajan');
            console.log('Logged in as '+ name);
            expect(element(by.css('.navbar-title')).getText()).toMatch("SEABIRD TOURISTS");
        });
    }*/

    /* For Vene.in */
    /*if(username === 'bala@machinetalk.io') {
        this.loginFunction(username, password);
        this.profileID.getText().then(function(name){
            expect(name).toMatch('Bala Vignesh');
            console.log('Logged in as '+ name);
            expect(element(by.css('.navbar-title')).getText()).toMatch("RAJESH TRANSPORTS");
        });
    }*/

    /*if(username === 'bala@vene.in') {
        this.loginFunction(username, password);
        this.profileID.getText().then(function(name){
            expect(name).toMatch('Bala');
            console.log('Logged in as '+ name);
            expect(element(by.css('.navbar-title')).getText()).toMatch("S.S.K.P TRANSPORT");
        });
    }*/

    /* For Seabird Tourists */
    if(username === 'seabird@vene.in') {
        this.loginFunction(username, password);
        expect(browser.getLocationAbsUrl()).toMatch("/dashboard");
        this.profileID.getText().then(function(name){
            expect(name).toMatch('Seabird Admin');
            console.log('Logged in as '+ name);
            expect(element(by.css('.logo-text')).getText()).toMatch("Seabird Tourists");
            expect(element(by.css('#wrapper > nav > div.navbar-header > a > img')).isPresent()).toBeFalsy();
        });
    }

    /* For KPN Travels */
    if(username === 'bala@machinetalk.io') {
        this.loginFunction(username, password);
        expect(browser.getLocationAbsUrl()).toMatch("/list/tyre");
        this.profileID.getText().then(function(name){
            expect(name).toMatch('Bala Vignesh S');
            console.log('Logged in as '+ name);
            //expect(element(by.css('#wrapper > nav > div.navbar-header > a > span')).getText()).toMatch("KPN");
            expect(element(by.css('.logo-text')).getText()).toMatch("KPN");
            expect(element(by.css('#wrapper > nav > div.navbar-header > a > img')).isPresent()).toBeFalsy();
        });
    }

    /* For Sam Tours & Travels */
    if(username === 'workshop@samtoursntravels.com') {
        this.loginFunction(username, password);
        this.profileID.getText().then(function(name){
            expect(name).toMatch('Workshop');
            console.log('Logged in as '+ name);
            expect(element(by.css('.logo-text')).getText()).toMatch("Sam Tourists");
            expect(element(by.css('#wrapper > nav > div.navbar-header > a > img')).isPresent()).toBeFalsy();
        });
    }

    /* For SSKP Logistics */
    if(username === 'admin@sskp.com') {
        this.loginFunction(username, password);
        this.profileID.getText().then(function(name){
            expect(name).toMatch('SSKP Admin');
            console.log('Logged in as '+ name);
            expect(element(by.css('.logo-text')).getText()).toMatch("SSKP Logistics");
            expect(element(by.css('#wrapper > nav > div.navbar-header > a > img')).isPresent()).toBeFalsy();
        });
    }

    /* For Infant's Travels */
    if(username === 'admin@infants.com') {
        this.loginFunction(username, password);
        this.profileID.getText().then(function(name){
            expect(name).toMatch('Admin');
            console.log('Logged in as '+ name);
            expect(element(by.css('.logo-text')).getText()).toMatch("Infants Travels");
            expect(element(by.css('#wrapper > nav > div.navbar-header > a > img')).isPresent()).toBeFalsy();
        });
    }
};