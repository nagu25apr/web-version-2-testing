'use strict';

var login = require('./login.js');
var logout = require('./logout.js');
var verify = require('./sidebarLinks.js');
var validateDashboard = require('./dashboardPage.js');
exports.veneVersionTwoUsing = function(username, password){
    describe('version 02 User ', function(){
        iit('should be able to log in with the given credentials', function(){
            browser.get('/');
            login.using(username, password);
        });
    });

    /* Verification of Links and Navigations */
    describe('version 02 ', function(){
        iit('should verify all the available links under the dashboard alert panel', function(){
            if(username !== 'bala@machinetalk.io')  {
                verify.dashboardLink();
            }
        });
        it('should verify all the available menus are available in the sidebar',function(){
            verify.sidebarContents();
        });

        it('should validate the track link available in the sidebar',function(){
            if(username !== 'bala@machinetalk.io')  {
                verify.vehicleTrackLink();
            }else{
                console.log('This login does not have access to Tracking Module');
            }
        });
        it('should validate the trip menu items in the side bar',function(){
            verify.tripMenuItems();
        });
        it('should validate the tyre menu items in the side bar',function(){
            if(username === 'bala@machinetalk.io')  {
                verify.tyreMenuItems();
            }
        });
        it('should validate the maintenance menu items and sub-menus in the side bar',function(){
            if(username !== 'bala@machinetalk.io')  {
                verify.maintenanceMenuItems();
                verify.maintenanceServiceMenuItems();
                verify.maintenanceTyreMenuItems();
                verify.maintenanceInventoryMenuItems();
            }
        });
        it('should validate the renewal menu items in the side bar',function(){
            if(username !== 'bala@machinetalk.io')  {
                verify.renewalMenuItems();
            }
        });
        it('should validate the fuel menu and sub-menus in the side bar',function(){
            if(username !== 'bala@machinetalk.io')  {
                verify.fuelMenuItems();
                verify.fuelEconomyMenuItems();
            }
        });
        it('should validate the complaints menu items in the side bar',function(){
            if(username !== 'bala@machinetalk.io')  {
                verify.complaintsMenuItems();
            }
        });
        it('should validate the finance menu items in the side bar',function(){
            if(username !== 'bala@machinetalk.io')  {
                verify.financeMenuItems();
            }
        });
        it('should validate the employees menu items in the side bar',function(){
            if(username !== 'bala@machinetalk.io')  {
                verify.employeesMenuItems();
            }
        });
        it('should validate the breakdown menu items in the side bar',function(){
            if(username !== 'bala@machinetalk.io')  {
                verify.breakdownMenuItems();
            }
        });
        it('should validate the reports menu items in the side bar',function(){
            if(username !== 'bala@machinetalk.io')  {
                verify.reportsMenuItems();
            }
        });
        it('should validate the setup menu and sub-menus in the side bar',function(){
            verify.setupMenuItems();
            verify.companySetupMenuItems();
            verify.vehicleSetupMenuItems();
            verify.consignmentSetupMenuItems();
            verify.customerSetupMenuItems();
            verify.locationSetupMenuItems();
            verify.tripSetupMenuItems();
            verify.tyreSetupMenuItems();
            verify.inventorySetupMenuItems();
            verify.serviceSetupMenuItems();
            verify.renewalSetupMenuItems();
            verify.fuelSetupMenuItems();
            verify.routeSetupMenuItems();
            verify.financeSetupMenuItems();
            verify.employeeSetupMenuItems();
        });
    });
    describe('Dashboard Page', function(){
        if(username !== 'bala@machinetalk.io'){
            validateDashboard.pageElements();
        }
    });
    describe('version 02 User ', function(){
        iit('should be able to log out from vene', function(){
            logout.from(username);
        });
    });
};