exports.config = {
  // The address of a running selenium server.
  seleniumAddress: 'http://localhost:4444/wd/hub',

  // Spec patterns are relative to the location of this config.
  specs: [
    'spec/*.spec.js'
  ],

  onPrepare: function() {
    browser.driver.manage().window().maximize();
  },

  'browserName': 'chrome',

  //multiCapabilities: [{
  //  'browserName': 'firefox'
  //}, {
  //  'browserName': 'chrome'
  //}],


  // A base URL for your application under test. Calls to protractor.get()
  // with relative paths will be prepended with this.
  //baseUrl: 'http://vene.in',
  baseUrl: 'http://app.vene.in',
  //baseUrl: 'http://app.vene.in:9000',

  jasmineNodeOpts: {
    onComplete: null,
    isVerbose: false,
    showColors: true,
    includeStackTrace: true,
    defaultTimeoutInterval: 500000
  }
};
